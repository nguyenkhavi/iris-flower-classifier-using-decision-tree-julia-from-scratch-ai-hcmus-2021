# 19120715
# Kha-Vi Nguyen
# Lab 3: Decision tree (ID3) on iris flowers dataset


using CSV
using DataFrames

MINIMUM_SAMPLE_SIZE = 3
MAX_TREE_DEPTH = 4

function read(path)
    return CSV.read(path, DataFrame)
end

function max_information_gain(df)

    max_info_gain = 0
    max_info_gain_split = 0
    max_info_gain_feature = ""
    features = filter((name) -> name != "species", names(df))

    for (index, feature) in enumerate(features)  # test all features
        # test all possible values as split limits
        for (idx, split) in enumerate(df[!, feature])
            # calculate gain
            split_info_gain = info_gain(feature, split, df)
            # println(feature,split, split_info_gain)
            if split_info_gain >= max_info_gain
                max_info_gain = split_info_gain
                max_info_gain_feature = feature
                max_info_gain_split = split
            end
        end
    end

    return max_info_gain, max_info_gain_feature, max_info_gain_split
end

function info_gain(feature, split, df)

    # split set and calculate probabilities that elements are in the splits
    set_smaller = filter(row -> row[feature] < split, df)
    p_smaller = nrow(set_smaller) / nrow(df)
    set_greater_equals = filter(row -> row[feature] >= split, df)
    p_greater_equals = nrow(set_greater_equals) / nrow(df)

    # calculate information gain
    info_gain = entropy(df)
    info_gain -= p_smaller * entropy(set_smaller)
    info_gain -= p_greater_equals * entropy(set_greater_equals)

    return info_gain
end

function entropy(df)

    if nrow(df) == 0
        return 0
    end

    target = "species"
    target_values = Set(df[!, target])

    data_entropy = 0
    for (index, val) in enumerate(target_values)

        # calculate the probability p that an element in the set has the value val
        p = nrow(filter(row -> row[target] == val, df)) / nrow(df)
        if p > 0
            data_entropy += -p * log(2, p)
        end
    end
    return data_entropy
end

function build_node(df, tree_depth)
    node = Dict()
    node["is_leaf"] = false
    node["depth"] = tree_depth
    if tree_depth < MAX_TREE_DEPTH && nrow(df) >= MINIMUM_SAMPLE_SIZE && length(Set([elem for (index, elem) in enumerate(df[!, "species"])])) > 1
        # get feature and split with highest information gain
        max_gain, feature, split = max_information_gain(df)
        # test if information gain is greater than 0 (another stopping criterion)
        if max_gain > 0
            # split tree
            node["split"] = split
            node["split_feature"] = feature

            # create chrildren
            left_df = filter(row -> row[feature] < split, df)
            right_df = filter(row -> row[feature] >= split, df)
            left_child = build_node(left_df, tree_depth + 1)
            right_child = build_node(right_df, tree_depth + 1)

            node["left_child"] = left_child
            node["right_child"] = right_child
        else
            node["is_leaf"] = true
        end
    else
        node["is_leaf"] = true
    end
    if node["is_leaf"] == true
        # prediction of leaf is the most common class in training_set
        setosa_count = versicolor_count = virginica_count = 0
        # for elem in training_set:
        #     if elem["species"] == "Iris-setosa":
        #         setosa_count += 1
        #     elif elem["species"] == "Iris-versicolor":
        #         versicolor_count += 1
        #     else:
        #         virginica_count += 1
        setosa_count = nrow(filter(row -> row.species == "Iris-setosa", df))
        versicolor_count = nrow(filter(row -> row.species == "Iris-versicolor", df))
        virginica_count = nrow(filter(row -> row.species == "Iris-virginica", df))

        dominant_class = "Iris-setosa"
        dom_class_count = setosa_count
        if versicolor_count >= dom_class_count
            dom_class_count = versicolor_count
            dominant_class = "Iris-versicolor"
        end
        if virginica_count >= dom_class_count
            dom_class_count = virginica_count
            dominant_class = "Iris-virginica"
        end
        node["prediction"] = dominant_class
    end
    return node
end


function merge_leaves(node)
    if !node["is_leaf"]
        node["left_child"] = merge_leaves(node["left_child"])
        node["right_child"] = merge_leaves(node["right_child"])
        if node["left_child"]["is_leaf"] && node["right_child"]["is_leaf"] && node["left_child"]["prediction"] == node["right_child"]["prediction"]
            node["is_leaf"] = true
            node["prediction"] = node["left_child"]["prediction"]
        end
    end
    return node
end

function print_node(node, prefix)
    if node["is_leaf"]
        print("\t"^node["depth"])
        print(prefix * node["prediction"])
        println()
    else
        print("\t"^node["depth"])
        print(prefix * node["split_feature"] * " < " * string(node["split"]) * " ?")
        println()
        print_node(node["left_child"], "[True] ")
        print_node(node["right_child"], "[False] ")
    end
end

function predict(node, sample)
    if node["is_leaf"]
        return node["prediction"]
    else
        if sample[node["split_feature"]] < node["split"]
            return predict(node["left_child"], sample)
        else
            return predict(node["right_child"], sample)
        end
    end
end

function train_test_split(df, test_size = 1 / 3, shuffle = true)
    if shuffle
        # suffle df 
        df = df[rand(1:nrow(df), nrow(df)), :]
    end

    break_point = floor(Int8, (1 - test_size) * nrow(df))

    train_df = df[1:break_point, :]
    test_df = df[break_point+1:nrow(df), :]

    return (train_df, test_df)
end

df = read("iris.csv")

# tran, test split
(train_df, test_df) = train_test_split(df)
println("Train set length: " * string(nrow(train_df)))
println("Test set length: " * string(nrow(test_df)))
# train model
clf = build_node(train_df, 0)

# merge leaves with the same prediction
clf = merge_leaves(clf)

# validate model
global acc = 0
for sample in eachrow(test_df)
    if sample["species"] == predict(clf, sample)
        global acc += (1 / nrow(test_df)) * 100
    end
end

println("__________________Decision Tree___________________")
print_node(clf, "")
println("__________________________________________________")

println("Accuracy on test set: " * string(round(acc, digits = 2)) * "%")

